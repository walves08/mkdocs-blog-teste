---
date: 2023-03-03
tags:
  - sas
  - sas-syntax
---

# SAS Syntax

<!-- more -->


- if else syntax


``` sas title="if else syntax" linenums="1"

/* Com um statement apenas */
DATA new_dataset;
    SET old_dataset;

    IF variable1 = 'Yes' THEN result = 1;
    ELSE IF variable1 = 'No' THEN result = 0;
    ELSE result = .;

RUN;



/* Com mais de um statement */
DATA new_dataset;
    SET old_dataset;

    IF variable1 = 'Yes' THEN DO;
        result1 = 1;
        result2 = 2;
    END;
    ELSE IF variable1 = 'No' THEN DO;
        result1 = 0;
        result2 = 0;
    END;
    ELSE DO;
        PUT 'Invalid value for variable1.';
        STOP;
    END;

RUN;

```




- libnames


``` sas title="criando libname" linenums="1"

/* Com authdomain */
LIBNAME DADOS DB2 DATABASE=bancodedados SCHEMA=schema AUTHDOMAIN=usuario;

/* Com usuário e senha */
LIBNAME PG postgres SERVER='192.168.0.1' DATABASE='DATABASE' PORT='5432' USER='postgres' PASS='SENHA' schema='SCHEMA';

/* Para uma pasta */
LIBNAME DADOS '/CAMINHO/PARA/PASTA';

/* Para várias pastas com uma mesma LIBNAME */
LIBNAME DADOS ('/CAMINHO/PARA/PASTA1', '/CAMINHO/PARA/PASTA2');

/* Limpar todas as LIBNAMEs */
LIBNAME _ALL_ CLEAR;

```


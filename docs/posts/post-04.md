---
date: 2023-04-04
tags:
  - sas
  - otimização
---


# Otimizando consultas

<!-- more -->


- existem casos em que a tabela já veio com os campos ordenado sem ter passado por uma ordenação.

``` sas title="indicar tabela já ordenada" linenums="1"

proc sql;
    select *
    from Scores(sortedby=subject_id visit) T1
    natural inner join Surgery(
        sortedby=subject_id visit 
        rename=(sid=subject_id)
    ) T2;
quit;
```



- cláusula `where` no `from`.

``` sas linenums="1"

    
    proc sql;
        create table scores_surglt16 as
        select 
            T1.*,
            case when ILV2.Visit gt 0 then 'Y' end as Surgery_Flag
        from Scores as T1
        left join ( 
            select *
            from Surgery
            where Visit lt 16
        ) as ILV2
        on T1.visit=ILV2.visit 
        and T1.subject_id=ILV2.sid;
    quit;

    /* a consulat acima pode ser feita de modo menos verboso assim */

    proc sql;
        create table scores_surglt16_2 as
        select 
            T1.*,
            case when T2.Visit gt 0 then 'Y' end as Surgery_Flag
        from Scores T1
        natural left join Surgery(
            where=(visit lt 16)
            rename=(sid=subject_id)
        ) T2;
    quit;
```


- cláusula `where` no `from` com prefixo nas colunas.

``` sas linenums="1"

    proc sql;

        create table ex11a as
        select *
        from scores
        where subject_id like '100-%' 
        or subject_id like '20%';

        /* pode ser dessa forma */

        create table ex11b as
        select *
        from scores(where=(subject_id in: ('100-','20')));

    quit;

```

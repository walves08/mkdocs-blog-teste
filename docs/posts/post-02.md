---
date: 2023-02-02
tags:
  - javascript
  - csv
  - dataviz
---

# Display CSV file in HTML table using Javascript.

<!-- more -->

``` csv title="Employee.csv" linenums="1"
Employee Id, Employee Name, Age, Email Id, Date of Joining
101,Rahul Singh,25,rahulsingh@gmail.com,20-08-2014
102,Neeraj Kumar,30,neeraj.k@gmail.com,05-01-2012
103,Raj Kumar Yadav,27,rajkumaryadav12@gmail.com,15-12-2017
```


``` javascript title="Display CSV file in HTML table using Javascript" linenums="1"

<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <title>CSV to Bootstrap Table | Javacodepoint</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
  </head>
  <body>
  <div class="container">
    <h3>Upload a CSV file to display in Bootstrap Table</h3>
    <!-- Input element to upload an csv file -->
    <input type="file" id="file_upload" />
    <button onclick="upload()" class="btn btn-primary">Upload</button>  
    <br>
    <br>
    <!-- table to display the csv data -->
    <table class="table table-bordered table-striped" id="display_csv_data"></table>
 </div>
    <script>
     
      // Method to upload a valid csv file
      function upload() {
        var files = document.getElementById('file_upload').files;
        if(files.length==0){
          alert("Please choose any file...");
          return;
        }
        var filename = files[0].name;
        var extension = filename.substring(filename.lastIndexOf(".")).toUpperCase();
        if (extension == '.CSV') {
            //Here calling another method to read CSV file into json
            csvFileToJSON(files[0]);
        }else{
            alert("Please select a valid csv file.");
        }
      }
       
      //Method to read csv file and convert it into JSON 
      function csvFileToJSON(file){
          try {
            var reader = new FileReader();
            reader.readAsBinaryString(file);
            reader.onload = function(e) {
                var jsonData = [];
                var headers = [];
                var rows = e.target.result.split("\r\n");               
                for (var i = 0; i < rows.length; i++) {
                    var cells = rows[i].split(",");
                    var rowData = {};
                    for(var j=0;j<cells.length;j++){
                        if(i==0){
                            var headerName = cells[j].trim();
                            headers.push(headerName);
                        }else{
                            var key = headers[j];
                            if(key){
                                rowData[key] = cells[j].trim();
                            }
                        }
                    }
                     
                    if(i!=0){
                        jsonData.push(rowData);
                    }
                }
                  
                //displaying the json result into table
                displayJsonToTable(jsonData);
                }
            }catch(e){
                console.error(e);
            }
      }
       
      //Method to display the data in Table
      function displayJsonToTable(jsonData){
        var table=document.getElementById("display_csv_data");
        if(jsonData.length>0){
            var headers = Object.keys(jsonData[0]);
            var htmlHeader='<thead><tr>';
             
            for(var i=0;i<headers.length;i++){
                htmlHeader+= '<th>'+headers[i]+'</th>';
            }
            htmlHeader+= '<tr></thead>';
             
            var htmlBody = '<tbody>';
            for(var i=0;i<jsonData.length;i++){
                var row=jsonData[i];
                htmlBody+='<tr>';
                for(var j=0;j<headers.length;j++){
                    var key = headers[j];
                    htmlBody+='<td>'+row[key]+'</td>';
                }
                htmlBody+='</tr>';
            }
            htmlBody+='</tbody>';
            table.innerHTML=htmlHeader+htmlBody;
        }else{
            table.innerHTML='There is no data in CSV';
        }
      }
    </script>
  </body>
</html>
```

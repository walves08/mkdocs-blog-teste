---
date: 2023-01-01
tags:
  - sas
  - sas-anomes
  - sas-datastep
---

# Gerar anomes entre duas datas

<!-- more -->

``` sas title="Gerar anomes entre duas datas" linenums="1"
DATA DATAS;
    FORMAT HOJE YYMMN6.;
    HOJE = TODAY();
    DO WHILE (HOJE >= INTNX('MONTH', TODAY(), -3));
        OUTPUT;
        HOJE = INTNX('MONTH', HOJE, -1);
    END;
RUN;
```
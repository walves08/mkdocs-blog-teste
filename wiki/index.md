

# TIL


> 20240102

#sas

``` sas title="Gerar anomes entre duas datas" linenums="1"
DATA DATAS;
    FORMAT HOJE YYMMN6.;
    HOJE = TODAY();
    DO WHILE (HOJE >= INTNX('MONTH', TODAY(), -3));
        OUTPUT;
        HOJE = INTNX('MONTH', HOJE, -1);
    END;
RUN;
```




> 20231206

Display CSV file in HTML table using Javascript.

``` csv title="Employee.csv" linenums="1"
Employee Id, Employee Name, Age, Email Id, Date of Joining
101,Rahul Singh,25,rahulsingh@gmail.com,20-08-2014
102,Neeraj Kumar,30,neeraj.k@gmail.com,05-01-2012
103,Raj Kumar Yadav,27,rajkumaryadav12@gmail.com,15-12-2017
```


``` javascript title="Display CSV file in HTML table using Javascript" linenums="1"

<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <title>CSV to Bootstrap Table | Javacodepoint</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
  </head>
  <body>
  <div class="container">
    <h3>Upload a CSV file to display in Bootstrap Table</h3>
    <!-- Input element to upload an csv file -->
    <input type="file" id="file_upload" />
    <button onclick="upload()" class="btn btn-primary">Upload</button>  
    <br>
    <br>
    <!-- table to display the csv data -->
    <table class="table table-bordered table-striped" id="display_csv_data"></table>
 </div>
    <script>
     
      // Method to upload a valid csv file
      function upload() {
        var files = document.getElementById('file_upload').files;
        if(files.length==0){
          alert("Please choose any file...");
          return;
        }
        var filename = files[0].name;
        var extension = filename.substring(filename.lastIndexOf(".")).toUpperCase();
        if (extension == '.CSV') {
            //Here calling another method to read CSV file into json
            csvFileToJSON(files[0]);
        }else{
            alert("Please select a valid csv file.");
        }
      }
       
      //Method to read csv file and convert it into JSON 
      function csvFileToJSON(file){
          try {
            var reader = new FileReader();
            reader.readAsBinaryString(file);
            reader.onload = function(e) {
                var jsonData = [];
                var headers = [];
                var rows = e.target.result.split("\r\n");               
                for (var i = 0; i < rows.length; i++) {
                    var cells = rows[i].split(",");
                    var rowData = {};
                    for(var j=0;j<cells.length;j++){
                        if(i==0){
                            var headerName = cells[j].trim();
                            headers.push(headerName);
                        }else{
                            var key = headers[j];
                            if(key){
                                rowData[key] = cells[j].trim();
                            }
                        }
                    }
                     
                    if(i!=0){
                        jsonData.push(rowData);
                    }
                }
                  
                //displaying the json result into table
                displayJsonToTable(jsonData);
                }
            }catch(e){
                console.error(e);
            }
      }
       
      //Method to display the data in Table
      function displayJsonToTable(jsonData){
        var table=document.getElementById("display_csv_data");
        if(jsonData.length>0){
            var headers = Object.keys(jsonData[0]);
            var htmlHeader='<thead><tr>';
             
            for(var i=0;i<headers.length;i++){
                htmlHeader+= '<th>'+headers[i]+'</th>';
            }
            htmlHeader+= '<tr></thead>';
             
            var htmlBody = '<tbody>';
            for(var i=0;i<jsonData.length;i++){
                var row=jsonData[i];
                htmlBody+='<tr>';
                for(var j=0;j<headers.length;j++){
                    var key = headers[j];
                    htmlBody+='<td>'+row[key]+'</td>';
                }
                htmlBody+='</tr>';
            }
            htmlBody+='</tbody>';
            table.innerHTML=htmlHeader+htmlBody;
        }else{
            table.innerHTML='There is no data in CSV';
        }
      }
    </script>
  </body>
</html>
```





## SAS


- if else syntax


``` sas title="if else syntax" linenums="1"

/* Com um statement apenas */
DATA new_dataset;
    SET old_dataset;

    IF variable1 = 'Yes' THEN result = 1;
    ELSE IF variable1 = 'No' THEN result = 0;
    ELSE result = .;

RUN;



/* Com mais de um statement */
DATA new_dataset;
    SET old_dataset;

    IF variable1 = 'Yes' THEN DO;
        result1 = 1;
        result2 = 2;
    END;
    ELSE IF variable1 = 'No' THEN DO;
        result1 = 0;
        result2 = 0;
    END;
    ELSE DO;
        PUT 'Invalid value for variable1.';
        STOP;
    END;

RUN;

```




- libnames


``` sas title="criando libname" linenums="1"

/* Com authdomain */
LIBNAME DADOS DB2 DATABASE=bancodedados SCHEMA=schema AUTHDOMAIN=usuario;

/* Com usuário e senha */
LIBNAME PG postgres SERVER='192.168.0.1' DATABASE='DATABASE' PORT='5432' USER='postgres' PASS='SENHA' schema='SCHEMA';

/* Para uma pasta */
LIBNAME DADOS '/CAMINHO/PARA/PASTA';

/* Para várias pastas com uma mesma LIBNAME */
LIBNAME DADOS ('/CAMINHO/PARA/PASTA1', '/CAMINHO/PARA/PASTA2');

/* Limpar todas as LIBNAMEs */
LIBNAME _ALL_ CLEAR;

```





- existem casos em que a tabela já veio com os campos ordenado sem ter passado por uma ordenação.

``` sas title="indicar tabela já ordenada" linenums="1"

proc sql;
    select *
    from Scores(sortedby=subject_id visit) T1
    natural inner join Surgery(
        sortedby=subject_id visit 
        rename=(sid=subject_id)
    ) T2;
quit;
```



- cláusula `where` no `from`.

``` sas linenums="1"

    
    proc sql;
        create table scores_surglt16 as
        select 
            T1.*,
            case when ILV2.Visit gt 0 then 'Y' end as Surgery_Flag
        from Scores as T1
        left join ( 
            select *
            from Surgery
            where Visit lt 16
        ) as ILV2
        on T1.visit=ILV2.visit 
        and T1.subject_id=ILV2.sid;
    quit;

    /* a consulat acima pode ser feita de modo menos verboso assim */

    proc sql;
        create table scores_surglt16_2 as
        select 
            T1.*,
            case when T2.Visit gt 0 then 'Y' end as Surgery_Flag
        from Scores T1
        natural left join Surgery(
            where=(visit lt 16)
            rename=(sid=subject_id)
        ) T2;
    quit;
```


- cláusula `where` no `from` com prefixo nas colunas.

``` sas linenums="1"

    proc sql;

        create table ex11a as
        select *
        from scores
        where subject_id like '100-%' 
        or subject_id like '20%';

        /* pode ser dessa forma */

        create table ex11b as
        select *
        from scores(where=(subject_id in: ('100-','20')));

    quit;

```



- renomeando várias colunas com sufixo numérico 


``` sas linenums="1"

proc sql;
    create table Baseline_A_1 as
    select 
        subject_id,
        a1 as base_a1,
        /* .. etc. .. , */
        a10 as base_a10
    from Scores
    where visit=1;

    /* pode ser feito dessa forma */

    create table baseline_A_2 as
    select *
    from Scores(rename=(a1-a10=base_a1-base_a10) drop=b:)
    where visit=1;
quit;

```

